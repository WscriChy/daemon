#ifndef keyboardHook
#define keyboardHook

#include "visibility.hpp"

#include <windows.h>

LRESULT
Hooks_PUBLIC
CALLBACK
keyboardHookCallback(int    nCode,
                     WPARAM wParam,
                     LPARAM lParam);

HHOOK
Hooks_PUBLIC
installKeyboardHook(HINSTANCE dllInstance);

BOOL
Hooks_PUBLIC
uninstallKeyboardHook(HHOOK const& hook);

#endif
