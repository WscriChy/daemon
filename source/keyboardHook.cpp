#include "keyboardHook.hpp"

#include <cstdio>
#include <iostream>

#include <cstring>
#include <map>
#include <psapi.h>
#include <set>

struct Key {
  Key(UINT sc_, UINT vk_) : sc(sc_), vk(vk_) {}

  bool
  operator<(Key const& other) const {
    return sc < other.sc;
  }
  UINT sc;
  UINT vk;
};

struct AltCombinations : public std::map < Key, std::pair < Key, bool >> {
private:
  typedef std::map < Key, std::pair < Key, bool >> Base;

public:
  AltCombinations() :  Base(), n(0) {}
  int n;
};
AltCombinations altCombos;
typedef std::set<Key> Combinations;
Combinations vimCombos;

UINT escapeScanCode      = 1;
UINT escapeVirtualKey    = MapVirtualKey(escapeScanCode, MAPVK_VSC_TO_VK);
UINT backspaceScanCode   = 14;
UINT backspaceVirtualKey = MapVirtualKey(backspaceScanCode, MAPVK_VSC_TO_VK);
Key  semicolon           = { 39, MapVirtualKey(39, MAPVK_VSC_TO_VK) };
UINT germanShiftScanCode   = 86;
UINT germanShiftVirtualKey = MapVirtualKey(germanShiftScanCode,
                                           MAPVK_VSC_TO_VK);
UINT lshiftScanCode   = 42;
UINT lshiftVirtualKey = MapVirtualKey(lshiftScanCode, MAPVK_VSC_TO_VK);
UINT rshiftScanCode   = 54;
UINT rshiftVirtualKey = MapVirtualKey(rshiftScanCode, MAPVK_VSC_TO_VK);
UINT altScanCode      = 56;
UINT altVirtualKey    = MapVirtualKey(altScanCode, MAPVK_VSC_TO_VK);
UINT ctrlScanCode     = 29;
UINT ctrlVirtualKey   = MapVirtualKey(ctrlScanCode, MAPVK_VSC_TO_VK);

Key h = { 35, MapVirtualKey(35, MAPVK_VSC_TO_VK) };
Key j = { 36, MapVirtualKey(36, MAPVK_VSC_TO_VK) };
Key k = { 37, MapVirtualKey(37, MAPVK_VSC_TO_VK) };
Key l = { 38, MapVirtualKey(38, MAPVK_VSC_TO_VK) };

Key leftArrow = { 75, MapVirtualKey(75, MAPVK_VSC_TO_VK) };
Key bottomArrow = { 80, MapVirtualKey(80, MAPVK_VSC_TO_VK) };
Key topArrow = { 72, MapVirtualKey(72, MAPVK_VSC_TO_VK) };
Key rightArrow = { 77, MapVirtualKey(77, MAPVK_VSC_TO_VK) };

int isAltPressed = 0;

void
releaseAltSafe() {
  keybd_event(ctrlVirtualKey,
              ctrlScanCode,
              0,
              0);
  keybd_event(altVirtualKey,
              altScanCode,
              2,
              0);
  keybd_event(ctrlVirtualKey,
              ctrlScanCode,
              2,
              0);
  isAltPressed = 1;
}

void
pressAltSafe() {
  isAltPressed = 0;
  keybd_event(altVirtualKey,
              altScanCode,
              0,
              0);
}

void
getApplicationTitle(LPSTR& title,
                    UINT&  length) {
  HWND hwnd = GetForegroundWindow();
  length = GetWindowTextLength(hwnd) + 1;
  title  = new char[length];
  GetWindowTextA(hwnd, title, length);
  // DWORD processId;
  // GetWindowThreadProcessId(hwnd, &processId);
  // HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
  // false,
  // processId);
  // LPSTR exeName       = new char[5];
  // DWORD  exeNameLength = GetModuleFileNameExA(handle, 0, exeName, 5);
  // ((void)exeNameLength);

}

bool
isIgnoredAltCombo(Key const& key) {
  LPSTR title;
  UINT  length;
  getApplicationTitle(title, length);

  for (int i = length - 5; i > 0; --i) {
    //printf("%s\n", &title[i + 1]);
    if (title[i] == 'G') {
      if (std::strncmp("VIM", &title[i + 1], 2) == 0) {
        if (vimCombos.find(key) != vimCombos.end()) {
          return true;
        }
      }
      return false;
    } else if (title[i] == ' ') {
      return false;
    } else if (title[i] != 'V' &&
               title[i] != 'I' &&
               title[i] != 'M' &&
               title[i] != '1' &&
               title[i] != '2' &&
               title[i] != '3' &&
               title[i] != '4' &&
               title[i] != '5' &&
               title[i] != '6' &&
               title[i] != '7' &&
               title[i] != '8' &&
               title[i] != '9' &&
               title[i] != '0') {
      return false;
    }
  }

  return false;
}

LRESULT
CALLBACK
keyboardHookCallback(int    nCode,
                     WPARAM wParam,
                     LPARAM lParam) {
  LRESULT result = -1;

  if (nCode >= 0) {
    PKBDLLHOOKSTRUCT kbllHookStruct = (PKBDLLHOOKSTRUCT)lParam;

    // char buf[sizeof (kbllHookStruct->flags) * 8 + 1];
    // printf("bin: %16s\n", itoa(kbllHookStruct->flags, buf, 2));
    // FILE* file = fopen("E:\\log.log", "a+");
    // fprintf(file, "code %lu\n", kbllHookStruct->scanCode);
    // printf("code %lu scancode %lu\n",
    // kbllHookStruct->scanCode,
    // kbllHookStruct->vkCode);

    // fclose(file);
    bool isReleased = 128 & kbllHookStruct->flags;
    bool isAlt      = 32  & kbllHookStruct->flags;
    // bool isInjected = 16  & kbllHookStruct->flags;

    bool isLShift = (1 << (sizeof (SHORT) * 8)) & GetKeyState(VK_LSHIFT);

    bool isRShift = (1 << (sizeof (SHORT) * 8)) & GetKeyState(VK_RSHIFT);

    if (kbllHookStruct->scanCode == semicolon.sc) {
      if (isAlt) {} else if (isLShift) {} else if (isRShift) {} else {
        if (!isAltPressed) {
          keybd_event(backspaceVirtualKey,
                      backspaceScanCode,
                      isReleased ? 2 : 0,
                      0);

          return 1;
        }
      }
    }

    if (kbllHookStruct->scanCode == 58) { // CapsLock
      keybd_event(escapeVirtualKey,
                  escapeScanCode,
                  isReleased ? 2 : 0,
                  0);

      return 1;
    } else if (kbllHookStruct->scanCode == germanShiftScanCode) {
      keybd_event(lshiftVirtualKey,
                  lshiftScanCode,
                  isReleased ? 2 : 0,
                  0);

      return 1;
    } else if (kbllHookStruct->scanCode == altScanCode) {
      if (isReleased) {
        if (isAltPressed) {
          isAltPressed = 0;

          if (altCombos.n != 0) {
            for (auto key : altCombos) {
              key.second.second = true;
              ((void)key);
            }
            altCombos.n = 0;
          }
        }
      }
    } else {
      Key key = Key(kbllHookStruct->scanCode,
                    kbllHookStruct->vkCode);
      auto it = altCombos.find(key);

      if (it != altCombos.end() && !isIgnoredAltCombo(key)) {
        if (isAlt && !isReleased && altCombos.n == 0) {
          releaseAltSafe();
        }

        if (isAltPressed) {
          if (isReleased != it->second.second) {
            altCombos.n      += isReleased ? -1 : +1;
            it->second.second = isReleased;
          }

          if (isReleased) {
            if (altCombos.n == 0) {
              pressAltSafe();
            }
          }

          keybd_event(it->second.first.vk,
                      it->second.first.sc,
                      isReleased ? 2 : 0,
                      0);

          return 1;
        }
      }
    }
  }

  HHOOK ignoredHook = 0;
  result = CallNextHookEx(ignoredHook, nCode, wParam, lParam);

  return result;
}

HHOOK
installKeyboardHook(HINSTANCE dllInstance) {
  altCombos.insert(
    std::make_pair(h, std::make_pair(leftArrow, true)));
  altCombos.insert(
    std::make_pair(j, std::make_pair(bottomArrow, true)));
  altCombos.insert(
    std::make_pair(k, std::make_pair(topArrow, true)));
  altCombos.insert(
    std::make_pair(l, std::make_pair(rightArrow, true)));
  altCombos.insert(
    std::make_pair(semicolon, std::make_pair(semicolon, true)));

  vimCombos.insert(h);
  vimCombos.insert(j);
  vimCombos.insert(k);
  vimCombos.insert(l);

  HHOOK hook =
    SetWindowsHookEx(WH_KEYBOARD_LL,
                     (HOOKPROC)keyboardHookCallback,
                     dllInstance,
                     0);

  return hook;
}

BOOL
uninstallKeyboardHook(HHOOK const& hook) {
  altCombos.clear();
  vimCombos.clear();

  return UnhookWindowsHookEx(hook);
}
