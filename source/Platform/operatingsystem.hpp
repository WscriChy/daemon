#ifndef Uni_Platform_Platform
#define Uni_Platform_Platform

// Preprocessor {{{
// =============================================================================
#if                                                                            \
  defined (__CYGWIN__)                                                      || \
  defined (__CYGWIN32__)
#  define Uni_Platform_PLATFORM "Cygwin"
#  define Uni_Platform_PLATFORM_CYGWIN
#  define Uni_Platform_PLATFORM_UNIX
#  define Uni_Platform_PLATFORM_WINDOWS
#elif                                                                          \
  defined (_WIN16)                                                          || \
  defined (_WIN32)                                                          || \
  defined (_WIN64)                                                          || \
  defined (__WIN32__)                                                       || \
  defined (__TOS_WIN__)                                                     || \
  defined (__WINDOWS__)
#  define Uni_Platform_PLATFORM "Windows"
#  define Uni_Platform_PLATFORM_WINDOWS
#elif                                                                          \
  defined (macintosh)                                                       || \
  defined (Macintosh)                                                       || \
  defined (__TOS_MACOS__)                                                   || \
  (defined (__APPLE__) && defined (__MACH__))
#  define Uni_Platform_PLATFORM "Mac"
#  define Uni_Platform_PLATFORM_MAC
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (linux)                                                           || \
  defined (__linux)                                                         || \
  defined (__linux__)                                                       || \
  defined (__TOS_LINUX__)
#  define Uni_Platform_PLATFORM "Linux"
#  define Uni_Platform_PLATFORM_LINUX
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (__FreeBSD__)                                                     || \
  defined (__OpenBSD__)                                                     || \
  defined (__NetBSD__)                                                      || \
  defined (__bsdi__)                                                        || \
  defined (__DragonFly__)
#  define Uni_Platform_PLATFORM "BSD"
#  define Uni_Platform_PLATFORM_BSD
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (sun)                                                             || \
  defined (__sun)
#  define Uni_Platform_PLATFORM "Solaris"
#  define Uni_Platform_PLATFORM_SOLARIS
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (_AIX)                                                            || \
  defined (__TOS_AIX__)
#  define Uni_Platform_PLATFORM "AIX"
#  define Uni_Platform_PLATFORM_AIX
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (hpux)                                                            || \
  defined (_hpux)                                                           || \
  defined (__hpux)
#  define Uni_Platform_PLATFORM "HPUX"
#  define Uni_Platform_PLATFORM_HPUX
#  define Uni_Platform_PLATFORM_UNIX
#elif \
  defined (__QNX__)
#  define Uni_Platform_PLATFORM "QNX"
#  define Uni_Platform_PLATFORM_QNX
#  define Uni_Platform_PLATFORM_UNIX
#elif                                                                          \
  defined (unix)                                                            || \
  defined (__unix)                                                          || \
  defined (__unix__)
#  define Uni_Platform_PLATFORM "Unix"
#  define Uni_Platform_PLATFORM_UNIX
#endif

#ifndef Uni_Platform_PLATFORM
#  error "Current platform is not supported."
#endif
// =============================================================================
// }}} Preprocessor

#endif // Uni_Platform_Platform

// vim:ft=cpp:fenc=utf-8:ff=unix:ts=2:sw=2:tw=80:et:
