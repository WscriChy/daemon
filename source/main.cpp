#include "keyboardHook.hpp"

#include <QtWidgets/QApplication>
#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>
#include <QtWidgets/QSystemTrayIcon>

#include <iostream>
#include <windows.h>

QSystemTrayIcon* _tray;
QMenu*           _menu;
HHOOK            _hook;
QApplication* _application;

void
quit() {
  _application->quit();
  uninstallKeyboardHook(_hook);
  delete _tray;
  delete _menu;
}

void
createTray() {
  _tray = new QSystemTrayIcon();
  _tray->setIcon(QIcon(":icon.ico"));
  _menu = new QMenu();
  _tray->setContextMenu(_menu);

  QAction* quitAction = new QAction("&Quit", _menu);
  QObject::connect(quitAction, &QAction::triggered, quit);
  _menu->addAction(quitAction);
  _tray->show();
}

int
main(int argc, char* argv[]) {
  Q_INIT_RESOURCE(config);
  HMODULE dllInstance = GetModuleHandleA("Hooks.dll");

  int exitCode;

  if (dllInstance == 0) {
    DWORD lastError = GetLastError();
    std::cout << "Was not able to find the dll: "
              << lastError << std::endl;
    exitCode = -1;
  } else {
    _hook = installKeyboardHook(dllInstance);

    if (_hook == 0) {
      DWORD lastError = GetLastError();
      std::cout << "Was not able to install the hook: "
                << lastError << std::endl;
      exitCode = -1;
    } else {
      _application = new QApplication(argc, argv);
      _application->setWindowIcon(QIcon(":icon.ico"));
      createTray();
      exitCode = _application->exec();
      delete _application;
    }
  }

  return exitCode;
}
