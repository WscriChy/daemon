#ifndef visibility
#define visibility

#include "Platform/visibility.hpp"

#if defined (Hooks_SHARED) && !defined (Hooks_STATIC)
#  ifdef Hooks_EXPORTS
#    define Hooks_PUBLIC Uni_Platform_EXPORT
#  else
#    define Hooks_PUBLIC Uni_Platform_IMPORT
#  endif
#  define Hooks_PRIVATE Uni_Platform_LOCAL
#elif !defined (Hooks_SHARED) && defined (Hooks_STATIC)
#  define Hooks_PUBLIC
#  define Hooks_PRIVATE
#elif defined (Hooks_SHARED) && defined (Hooks_STATIC)
#  ifdef Hooks_EXPORTS
#    error "Cannot build as shared and static simultaneously."
#  else
#    error "Cannot link against shared and static simultaneously."
#  endif
#else
#  ifdef Hooks_EXPORTS
#    error "Choose whether to build as shared or static."
#  else
#    error "Choose whether to link against shared or static."
#  endif
#endif

#endif
